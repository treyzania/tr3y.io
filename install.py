#!/usr/bin/env python3

import os
import subprocess
import sys
import shutil
import pathlib

output = sys.argv[1]

STATICDIR = 'static'

print('exporting to \'' + output + '\'')

# Make sure the directory doesn't already exist.
if not os.path.exists(output):
    os.mkdir(output)
else:
    print('directory already exists!')
    sys.exit(1)

# Copy the static files
for dp, dns, fns in os.walk(STATICDIR):
    for fn in fns:
        if fn.endswith('~'):
            continue
        src = os.path.join(dp, fn)
        rel = os.path.relpath(src, STATICDIR)
        dest = os.path.join(output, rel)
        destparent = os.path.dirname(dest)
        if not os.path.exists(destparent):
            print('creating dir', destparent)
            os.makedirs(destparent, exist_ok=True)
        print('copying %s -> %s' % (src, dest))
        shutil.copy2(src, dest)

def render_toc(path):
    cmd = 'pandoc --from gfm --template pandoc-only-toc.template --toc --standalone --metadata title=- %s' % path
    print(cmd)
    p = subprocess.Popen(cmd.split(), shell=False, stdout=subprocess.PIPE)
    p.wait()
    out, _ = p.communicate()
    return str(out, 'utf8')

ARTICLE_FOOTER = """---

[Articles Index](/articles.html)
"""

# Convert and export the markdown files.
def export_markdown(path):
    infile = os.path.join('markdown', path)

    render_buf = []
    title = None
    with open(infile, 'r') as f:
        for i, l in enumerate(f.readlines()):
            if 'tz-insert-toc' in l:
                render_buf.append(render_toc(infile))
            elif 'tz-insert-footer' in l:
            	render_buf.append(ARTICLE_FOOTER)
            else:
                render_buf.append(l)
            if l.startswith('# ') and title is None:
                title = l[2:].strip()

    render_buf = ''.join(render_buf)

    wwwpath = os.path.splitext(path)[0] + '.html'
    ogurl = 'https://tr3y.io/' + wwwpath
    outfile = os.path.join(output, wwwpath)
    os.makedirs(os.path.dirname(outfile), exist_ok=True)

    cmd = [
        'pandoc',
        '--from', 'markdown',
        '--template', 'pandoc-article.template',
        '--standalone',
        '--include-in-header=mdheader.htm',
    ]

    cmd.append('--metadata')
    if title is not None:
        cmd.append('title=%s :: tr3y.io' % title)
        cmd.extend(['--metadata', 'ogtitle=' + title])
    else:
        name = os.path.splitext(os.path.basename(path))[0]
        cmd.append('title=%s :: tr3y.io' % name)

    if ogurl is not None:
        cmd.extend(['--metadata', 'ogurl=' + ogurl])

    # Add on the output and inputs.
    cmd.extend(['-o', outfile, '-'])

    print(' '.join(cmd))
    p = subprocess.Popen(cmd, shell=False, stdin=subprocess.PIPE)
    p.communicate(input=render_buf.encode())

for dp, dns, fns in os.walk('markdown'):
    for fn in fns:
        if fn.endswith('~'):
            continue
        export_markdown(os.path.relpath(os.path.join(dp, fn), 'markdown'))

