# tr3y.io

This is just a repository that I run `git push prod master` in when I update it
to show on my public website, available at http://tr3y.io/.  This is mostly
static content with just a little tiny bit of dynamic stuff to show statuses of
certain things.
