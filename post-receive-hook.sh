#!/bin/bash

set -e

read ref

cd ../..
echo $ref >> git-updates.log
./rebuild.sh tr3y.io
