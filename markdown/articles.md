# Articles

I've recently started writing a few articles.  Some of them are more and less
works-in-progress, but regardless you might find them useful.

There's some more articles that aren't in showable condition yet, but if you're
smart you might be able to figure out where to look to find the partial drafts.

Feel free to reach out to me if you'd like to discuss further.

## Tech

I have a lot of hot takes about technology so instead of trying to articulate
them every time someone wanted to talk about them, I just wrote these to
articulate them more thoroughly.

* [How to git gud](/articles/tech/gitgud.html) *"the git gud guide!"*
  * [Git gud projects](/articles/tech/gitgud-projects.html) (exercises to help self-taught programmers learn new concepts)
* [Project Ideas](/articles/tech/ideas.html) (larger, more open-ended projects I know I don't have time to work on that someone else should)
* [Why Linux?](/articles/mf/why-linux.html) (a little sloppy, I should rework this to be cleaner and with my current style)
* [reddit: What is to be Done?](/articles/tech/reddit.html)
* // TODO critique of the matrix protocol
* [A better chat protocol](/articles/tech/better-chat-protocol.html)
* [Mastodon and the Fediverse have already won](/articles/tech/fedi-won.html)

## Crypto

The crypto industry is pretty thoroughly full of frauds.  There's a whole lot to
be skeptical about, and lots of bad actors.  But it's also possible that it's
going to have a big impact on our lives despite anyones' objections, so it's
important to understand it, how it actually works, how we might use it to work
towards positive social change.  I would like to be optimistic.

* [Bitcoin zk-rollups](/articles/crypto/bitcoin-zk-rollups.html)
* Mirror: [Why delegated-type proof-of-stake is unsustainable](/articles/crypto/dpos-unsustainable.html) (not my article, reddit deleted the post for being "off-topic")
* [So you want to be a blockchain developer?](/articles/crypto/how2bloccchain.html)
* [Towards a better wallet framework](/articles/crypto/towards-better-wallets.html)
* [Governance vs Protocol Risks](/articles/crypto/gov-vs-proto-risk.html)
* [Defining "layer 2", once and for all](/articles/crypto/defining-layer-2.html)
* // TODO analysis of the fetishization of "ownership" of virtual "property"
* // TODO discussion of fake libertarians

