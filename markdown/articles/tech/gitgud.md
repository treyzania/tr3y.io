# How to git gud

*Revised 2024-07-07.*

This is a document I've been thinking about writing for quite a while.  A lot
of people that want to learn about programming or computer science today tend
to take a certain path through it and I would like to provide some framing that
a lot of people might find useful.  It's important to understand that computers
are not magical and everything is simple if you look at it long enough from the
right direction.  So what I'm sharing here is many of the topics I've had
experience with when learning to
[git gud](https://www.dictionary.com/e/slang/git-gud/), so that you can too.

**This article is pretty opinionated.**  It's not meant to not be.  But it's
probably a lot different than other guides/roadmaps/etc you've seen before.

This document isn't meant to teach you, it's meant to guide you.  You can take
a different path, especially if it means you're more engaged in the content and
feel more enriched.  Learning is always good.  You will have to Google many
things along the way, half the work is learning how to learn!

These steps aren't even meant to be taken linearly, if you achieve all of them
then I consider you to be "pretty good" and someone I would be happy to hire.
But if you specialize in a different direction, that's great, your skills are
still improving and your time is more valuable.  This isn't even *exactly* what
I've experienced, it's just enough to cover all of the skills I think are
valuable to have.

I'm saying this *prescriptively*, not *descriptively*.  If your goal is to jump
in and get a professional job as quickly as possible, you could take an easier
path and just go to a bootcamp for webdev.  But the odds are you wouldn't be as
broadly enriched, will be put into a standard-sized bucket when looking for
jobs, and will have a pit to dig yourself out of by relearning skills that
weren't properly taught to you in the first place.  Focusing your education on
just what's trendy to get a job doing is *not* the strategy you should be
taking, because if that's your priority you're going to compromise on rigor in
order to achieve some short term goal.  This article is meant to guide you to do
the opposite.

<!-- tz-insert-toc -->

## Foundations

Computer science is not *really* about computers or programming.  It's about
studying the way we model and transform data.  But the primary context we talk
about these ideas is in the context of programs, which are ran inside of the
computers made out of silicon and copper.  And people have connected those
computers together in interesting ways with strands of copper and glass.  So
unfortunately when we talk about computer science, we also tend to talk about
computers and programs and networks.

So to effectively use common languages to solve interesting problems, we have to
have solid understanding of the theory behind it to design effective programs.
And to apply abstract ideas to real-world problems, we need to be competent at
programming languages that give us the tools to *express* our ideas.  In this
way, there's a *dual nature* between computer science and software engineering.
It's hard to be skilled at one without the other.

In any case, there are two ends of the spectrum and many areas that exist along
it.  To start off with, a very "practical" language that everyone should know is
Python (and I mean *everyone* that has to spend a lot of time on their computers
for their job, not just CS/software people).  It's very commonly used in
teaching and has very broad applicability so it's an invaluable tool to have in
your belt.  But to develop a more thorough appreciation of the abstract side,
a good language to do *that* through is Racket.  See more on these below.  It's
important to understand that these are just the *starting points*.  While you
can write fairly sophisticated programs with them, you should always be looking
to expand your horizons once you've gained some comfortable level of competency
with them.

But more generally, there's many areas of skills that a competent computer
person might be interested in beyond narrow programming skills.  You don't have
to have all of these to be an expert at some specific domain, and there's a huge
amount of overlap between these so there isn't really a "jack of all trades,
master of none" effect.

But to summarize these areas non-exhaustively, and under each point how you'd 
approach it:

* competency at learning new programming languages
  * learn lots of languages and use them
  * especially focus on ideas in different domains
* understanding algorithmic thinking to solve a problem and designing data structures to suit a purpose
  * practice, taking on challenging problems
  * study other people's code
* programming language theory
  * implement theoretical ideas and apply them
  * learn languages that leverage more esoteric ideas
* software security
  * read case studies, vuln explanations, etc.
  * watch talks
* CPU architecture, at the level a casual assembly programmer might
  * comes through writing C, assembly
  * read about software vulns
  * watch talks
* computer hardware
  * upgrade your computer's RAM, hard drive, etc
  * build your own PC or server from consumer parts you buy on Newegg or at Microcenter
  * repair a PC you found in a dumpster
* using linux (and unix-line command line environments)
  * set up a server, running stuff on it
  * manage your linux desktop, since those skills are transferable
  * build unixy software
* computer networking
  * write network code!
  * manage your own local network
  * read case studies
* rudimentary web design
  * building a personal website for yourself
  * open the browser inspector to see how common websites work

By "case study" here I don't mean formal analyses of some subject.  I mean this
in a super unstructured way.  Tech blogs from startups that write projects we
write.  Something somebody in their basement might have figured out.  It's fun.

In case you haven't realized it yet, a lot of these points involve "do something
that depends on it".  Something unique about software design as a discipline is
that we get to practice it in our bedrooms.  This is unlike disciplines like
chemical or civil engineering, or to a lesser extent ones like mechanical or
electrical engineering.  The extents to which you can practice these at home
with your own resources is very different than the extent and nature you'd do it
at work.  With us, the scale is often smaller, but many bedroom projects became
wild successes, like Linux.

### Courses and books

For a very first-principles introduction that gradually introduces you to a very
good understanding of some fairly abstract topics, I suggest learning Racket
through
[How to Design Programs](https://htdp.org/2020-5-6/Book/part_preface.html).  It
has *wonderful* documentation.  Racket is a Scheme-like languages, and HTDP is
inspired by the older
[Structure and Interpretation of Computer programs](https://en.wikipedia.org/wiki/Structure_and_Interpretation_of_Computer_Programs).
These books start slow, but it's because they're being very principled about it.
HTDP is used to teach every freshman at my university and the teaching faculty
have taken a
[very principled](https://felleisen.org/matthias/Thoughts/Developing_Developers.html)
approach to designing the courses.  You might never learn Racket/Scheme/Lisp to
a professional level, but the concepts you learn from it *will* be useful in
everything else you do.

At the other end of the spectrum, for the more practical topics that you can
focus on *in the process of* learning Python, there's 

* [MIT OCW](https://ocw.mit.edu/search/?l=Undergraduate&q=6) (undergrad classes starting with 6)
  * specifically consider [Introduction to Computer Science and Programming](https://ocw.mit.edu/courses/6-00-introduction-to-computer-science-and-programming-fall-2008/pages/syllabus/) for Python
* [Harvard CS50](https://pll.harvard.edu/course/cs50-introduction-computer-science)

With some overlap, there's also
[The Missing Semester of Your CS Education](https://missing.csail.mit.edu/)
that covers a lot of *great* topics.  But of this, these lessons are the big
ones to focus on I think:

* using command line interfaces, including the data wrangling part
* Git
* security and cryptography

And for something more self-directed:

* Python's [first-party documentation](https://docs.python.org/3/tutorial/)
* [the Rust book](https://doc.rust-lang.org/book/)
* books for Python like [this one](https://automatetheboringstuff.com/) that might suit your fancy as well, there's ones like these for many languages

Please suggest me more if you have others!

### People and Blogs

Here's a list of blogs, people, channels, etc that are interesting characters to
check out, wherever they happen to crop up, since they're either really
informative or have interesting perspectives.

* Prominent people
  * Linus Torvalds (founder of the Linux project, you already know who he is)
  * [Cory Doctorow](https://pluralistic.net/) (blogger, author, advocate)
  * John Blow (game developer behind The Witness)\
    * [Preventing the Collapse of Civilization / Jonathan Blow (Thekla, Inc)](https://www.youtube.com/watch?v=ZSRHeXYDLko)
* Blogs
  * [xkcd](https://xkcd.com/) (comic strip, not really a *blog*)
  * [Secret Club](https://secret.club/)
  * [fail0verflow](https://fail0verflow.com/)
  * [nand2tetris](https://www.nand2tetris.org/)
  * [Alyssa Rosenzweig's blog](https://rosenzweig.io/) (on Apple silicon graphics driver and Asahi Linux)
  * [The Dolphin Emulator Blog](https://dolphin-emu.org/blog/)
  * [Google Project Zero](https://googleprojectzero.blogspot.com/) (Google's vulnerability research team's blog)
    * [A deep dive into an NSO zero-click iMessage exploit: Remote Code Execution](https://googleprojectzero.blogspot.com/2021/12/a-deep-dive-into-nso-zero-click.html)
  * [Xe Iaso's blog](https://xeiaso.net/blog/) (somewhat whimsical, but very deep topics)
  * [The Daily WTF](https://thedailywtf.com/) (fairly silly, but touches a lot of topics and gives example of bad code)
* Programming YouTubers
  * [LiveOverflow](https://www.youtube.com/@liveoverflow) (computer security, vuln explanations, etc.)
  * [Ben Eater](https://www.youtube.com/@BenEater) (electronics, assembly programming on DIY computers)
  * [Tsoding Daily](https://www.youtube.com/@TsodingDaily) (misc, programming streams)
  * [Acerola](https://www.youtube.com/@Acerola_t) (computer graphics)
  * [Randy](https://www.youtube.com/@randyprime) (humorous, gamedev)
  * [pannenkoek2012](https://www.youtube.com/@pannenkoek2012) (technical topics around Super Mario 64)
* News sites (I don't read these, usually I find links to them from Hacker News)
  * [Phoronix](https://www.phoronix.com/)
  * [The Register](https://www.theregister.com/)
  * [Bleeping Computer](https://www.bleepingcomputer.com/)
  * [TorrentFreak](https://torrentfreak.com/)
  * [404media](https://www.404media.co/) (a bit more mainstream)
  * [Hackaday](https://hackaday.com/)

Also, here's some specific articles and stuff that I think are pretty
interesting that aren't accounted for the above groupings:

* Hacking
  * [Detecting a PS2 Emulator: When 1*X does not equal X](https://fobes.dev/ps2/detecting-emu-vu-floats)
  * [Hacking Millions of Modems (and Investigating Who Hacked My Modem)](https://samcurry.net/hacking-millions-of-modems)
  * [Exploiting the iPhone 4, Part 1: Gaining Entry](https://axleos.com/exploiting-the-iphone-4-part-1-gaining-entry/)
  * [Operation Charlie: Hacking the MBTA CharlieCard from 2008 to Present](https://medium.com/@bobbyrsec/operation-charlie-hacking-the-mbta-charliecard-from-2008-to-present-24ea9f0aaa38)
* Education and cool tech
  * [Learning about distributed systems: where to start?](http://muratbuffalo.blogspot.com/2020/06/learning-about-distributed-systems.html?m=1)
  * [Why You Should Write Your Own Static Site Generator](https://arne.me/blog/write-your-own-ssg)
  * [Things Every Hacker Once Knew](http://www.catb.org/~esr/faqs/things-every-hacker-once-knew/)
  * [How Developers Stop Learning: Rise of the Expert Beginner](https://daedtech.com/how-developers-stop-learning-rise-of-the-expert-beginner/)
  * [How Discord Stores Trillions of Messages](https://discord.com/blog/how-discord-stores-trillions-of-messages) (technically interesting, however I very much dislike Discord)
* Tech politics
  * [Software Bugs That Cause Real-World Harm](https://pointersgonewild.com/2023/05/29/software-bugs-that-cause-real-world-harm/)
  * [I worry our Copilot is leaving some passengers behind](https://joshcollinsworth.com/blog/copilot)
  * [Is software getting worse?](https://stackoverflow.blog/2023/12/25/is-software-getting-worse/) (and linked articles)
  * [PSA: Do Not Use Services That Hate The Internet](https://www.jwz.org/blog/2022/11/psa-do-not-use-services-that-hate-the-internet/)
  * [The Failed Commodification Of Technical Work](https://ludic.mataroa.blog/blog/the-failed-commodification-of-technical-work/)
  * [Discord, or the Death of Lore](http://ascii.textfiles.com/archives/5509)
  * [Local-first software](https://www.inkandswitch.com/local-first/)
  * [The Anti-Ownership Ebook Economy](https://www.nyuengelberg.org/outputs/the-anti-ownership-ebook-economy/)
* The web
  * [The deskilling of web dev is harming the product but, more importantly, it’s damaging our health – this is why burnout happens](https://www.baldurbjarnason.com/2024/the-deskilling-of-web-dev-is-harming-us-all/)
  * [JavaScript Bloat in 2024](https://tonsky.me/blog/js-bloat/)
  * [Splitting the Web](https://ploum.net/2023-08-01-splitting-the-web.html)

These lists are *far* from complete, will add more as I get to them.

### Conferences

Talks from conferences can get very deep.  I constantly used to watch talks I
simply did not understand any of the details of, but gave me some broad
understanding of some area that I came back around to much later with a greater
appreciation for.  On the other hand, they can also can be very thought
provoking and prompt you to think about issues you may not have considered
otherwise.

* [Chaos Communication Congress](https://media.ccc.de/) ([yt mirror](https://www.youtube.com/@mediacccde))
* [DEFCON](https://www.youtube.com/@DEFCONConference)
* [Blackhat](https://www.youtube.com/@BlackHatOfficialYT)
* [Libreplanet](https://media.libreplanet.org/)
* [Game Developers Conference (GDC)](https://www.youtube.com/channel/UC0JB7TSe49lg56u6qH8y_MQ)
* [X.Org Developers Conference (XDC)](https://www.youtube.com/@XOrgFoundation) (serious innovation on the Linux desktop)

## Projects

Linked below (near the bottom of this section) is a large list of project ideas
that I moved onto its own page.  These are just starting points, don't treat
them as a list of tasks you have to check off.  If you find some other related
idea interesting or have some novel problem you just thought of that you think
you can solve, you absolutely should explore it!

Also, not all of these involve actually writing code, some only involve learning
some new CLI tools or service you should learn to configure.  I mostly avoid
discussing use of specific languages, too.  This is because *knowing the right
tool for the job* is something that's useful to learn on your own.  You might
want to attempt some project multiple times using different languages to get a
more natural feeling for which languages are better suited for different
use-cases.

<center>[**New Projects page here!**](/articles/tech/gitgud-projects.html)</center>

If you're a beginner then looking at this section might seen intimidating.
That's kinda on purpose, they're not all meant to be introductory.  But at least
a couple of these should feel approachable to you if you've dipped your toes
into programming enough to get a feel for it.  You'll probably have to read some
documentation or Wikipedia articles, but once you work through a couple things
then read over the list again and see what seems more approachable now with the
new skills you've learned.

### Advice on langauges

Python is so incredibly useful because of how multi-domain it is.  So for a lot
of these projects it's a good starting point.  But it might not be the *ideal*
language for some of them, as I mentioned above, so you'll eventually want to
learn others.

Beyond Python and Racket, these are the main languages that I think you should
focus on:

* C, to establish a good understanding of low-level software design that *everything* else is built on
* Rust, ideally after learning C, as a modern systems language designed by competent people
* Bash, you get this naturally in the process of learning to work with a Linux environment
* HTML/CSS, since it's so useful for publishing on the internet, and likely in conjunction with Python if you want to programmatically publish data or accepting user input

Other languages that are more niche that you may want to work with at some point
that could be worthwhile:

* OCaml, for a more abstract understanding of the ML family Rust is related to
* Scala, for a JVM-flavored exposure to Rust stuff
* Haskell, for lazy evaluation, monads, etc.
* C#, for gamedev, unfortunately
* Kotlin, for modern Android development
* Elixir, for highly scalable concurrent applications

Other languages you may want to learn to study their mistakes and flaws:

* JavaScript, only use it sparingly and when absolutely necessary for web UIs
* Go, but [only](https://fasterthanli.me/articles/i-want-off-mr-golangs-wild-ride) to understand its [many flaws](https://fasterthanli.me/articles/lies-we-tell-ourselves-to-keep-using-golang)
* C++, which has many historical reasons for existing the way it does
  * it's widely used in gamedev and a ton of legacy software, so if that's a direction you want to go in you'll want to be fairly competent at it
  * I suggest only approaching it after learning and becoming competent at C first, then Rust, and then learning C *again*

If you've read other guides, you'll probably see more focus on the web stack,
including JavaScript.  It's useful to know it, but it's very often overused,
and building everything to run in a web browser is limiting.  I don't think that
we should be encouraging new programmers to learn JS until they've reached a
certain level of competency.  It's very approachable with little background and
there's a lot of resources for doing that, but naively learning it teaches you
a lot of bad habits and its idiosyncrasies mislead you.

// TODO finish and link js article

## Tools

If you're someone that wants to work with computers every day, then you need
to care about your tools.  Tools help us get our work done, software tools are
just as important as physical tools.  And how are tools are designed has a big
impact on us and how we do our work, so it's important we put a lot of thought
into them.

### Software

Prefer FOSS wherever possible.  It's hard to avoid proprietary software in all
cases, especially if you want to play video games, but we can set ourselves up
such that we're not *reliant* on it.

#### Operating system

You should make it a goal to switch to using Linux as your daily driver.  This
isn't an unreasonable request, it's 2022, you don't have to be a
technically-minded person to use it anymore.  The benefits you get out of it
are not only being able to have more direct relationship with your system, but
you also take a big step to free yourself from the corporate-owned software
world and embrace the community-owned world.  It's more than just about its
technical merit, what's more important are the power relationships and the
underlying principles.

You don't have to start here.  You can get pretty far if you're on Windows.
While you don't *have* to open a shell to get stuff done anymore, you should
expect to to do many of the projects I've suggested later in this article, so
you might want to familiarize yourself with it on Windows first and then
switch.  Or not, if you're enthusiastic you can jump right in immediately.

It's easier to daily drive it today than it ever has been, in part thanks to
work by Valve (one of the few *not obviously evil* corporations) you even can
play lots of "officially-Windows-only" vibeo games on it, in many cases right
out the box in Steam.

MacOS is potentially more acceptable, and you could go farther with it without
hitting limits to your skills, but in this case you're still within Apple's
grasp and you're subject to their corporate agenda.

On mobile, similar thinking applies.  Make your next phone be a Pixel and put
an Android ROM like CalyxOS, CopperheadOS, GrapheneOS.  I'm not an expert on
Android so you should do a little of your own research to find the right
balance of de-Googling for you, but CalyxOS suits me well enough.

If you're a high school student, I suggest making the switch over the summer
before you start the next school year, especially if your next school year is
going to college.  Just before college is when I switched over to it as my
daily driver and it was a *great* choice.  If you're already in college, then
doing it between semesters like during winter break is probably a good time.

#### Web browsers

Chrome is owned by Google.  That shouldn't be a surprised.  *Chromium* is an
open source software project managed by Google.  I say *open source*, but only
in the strictest sense of the word.  Google makes unilateral decisions about
the direction of the project, to serve their corporate agenda.  There's a few
browsers such as Brave that are based on the Chromium codebase, so Google
indirectly control these too.  Since Google basically controls this much of the
web market share, they also basically control web standards and have a large
say over the World Wide Web Consortium that, on paper, is supposed to decide
things.

Additionally, Chrome phones home to Google everything about your browsing
activity and JS heap dumps, even in incognito mode.

Mozilla currently has some troubles, and only really exist due to funding from
Google as a tax writeoff and so they can have a defense against antitrust
lawsuits.  But Firefox is really the only mature web browser that is
justifiable to use.  There's
[a few tweaks](https://github.com/arkenfox/user.js/) you have to make to get it
to really respect your privacy, but this is a one-time-setup.

As for browser extensions, the most important single one is **uBlock Origin**,
most of the other adblockers take bribes to not block certain ads.  If you want
to support creators that seem to rely on advertising revenue, then it's better
for everyone if you just pay them directly through Patreon, Liberapay,
OpenCollective, or whatever other system they use.  Advertising is parasitic in
nature and we should not accept being distracted by it.

The other browser extensions that are useful:

* [Consent-O-Matic](https://github.com/cavi-au/Consent-O-Matic), to auto-close some cookie warnings
* [LibRedirect](https://github.com/libredirect/libredirect), to auto-redirect away from messy, bloated corporate social media frontends to lightweight proxies like Nitter
* [Bypass Paywalls](https://github.com/iamadamdev/bypass-paywalls-chrome), which can also be used on Firefox
* [NoScript](https://github.com/hackademix/noscript), if you're dedicated

// TODO elaborate

#### Text editors

Avoid VS Code, for many of the same reasons you should avoid Google Chrome.  If
you really want that style, then use [VS Codium](https://vscodium.com/) intead
to actually be using free software and not the clever bait-and-switch Microsoft
pulls.

Personally, I use Emacs, but it's not right for everyone.  Vim is also decent.
Both take some setup and getting used to but are more powerful and give you
more control than today's mainstream text editors.

// TODO elaborate

### Hardware

Choosing the right hardware can make daily-drivering Linux easier.  Some
high-end laptops even come with Linux preinstalled!  My rough reccomendations
are:

* [Pinebook](https://www.pine64.org/pinebook/)
* [Librem 14](https://puri.sm/products/librem-14/)
* [some of System76's laptops](https://system76.com/)
* Dell XPS Developer Edition (not as freedom-respecting, but still very good)
* various Thinkpads, find other suggestions
* try it out on an old laptop to maybe breathe new life into it (this option might be most viable if you're cheap)
* build your own desktop PC (with an AMD GPU)

The important thing to be aware of is to avoid Nvidia graphics cards.  Their
driver support is terrible and while they've taken steps in the right direction
recently, the weird setups you'd find on laptops don't make configuration
easier.  You probably don't need high-end graphics on a laptop, you can get by
with the integrated graphics on modern CPUs.

If you really want high-end graphics, I would suggest buying a cheaper laptop
and saving your money to build a desktop with an AMD graphics card.

I don't know of any regular desktop motherboards that support Coreboot or
Libreboot such that we can have privacy-respecting firmware, but that's a whole
different battle and kinda hard to try there yet.  Obviously it's more
reasonable to use a less freedom-respecting laptop that you already have than
pay for a new laptop that *does*, but not everyone is as purist as I aim to be
or you might have more money to spend than I do.

*Don't* buy an Apple computer.  They're nice and pretty, and the newer ARM ones
are very competitive with Intel/AMD based laptops, but that's not what matters
right now, it's an issue of ethical consumption.  Watch Louis Rossman's videos
on YouTube if you really want to go into the sheer degree of anticompetitive
bullshit they're capable of.

If you already own a Mac then that's totally okay, but but look into using
Asahi Linux when it's more mature.

If you're planning on doing anything with cryptocurrencies, you should also buy
a hardware wallet to store your funds on if you have more than a couple hundred
dollars worth.  This is kinda orthogonal to the other stuff, but it's just way
safer and it needs to be said.  Plus, some of them also can work a U2F device!

### Services

It's easier to switch the software you rely on that you run locally, but it's
harder to switch out the software you rely on that runs somewhere else.  So it
might take more of a lifestyle change to reduce your usage of those.

Generally you should look towards using self-hostable services, or at the very
least services that have fully free software clients.

Try these replacements:

* Gmail -> ProtonMail, Tutanota (and try Thunderbird if supported)
* Google Drive, iCloud, etc -> Nextcloud, Syncthing
* PayPal, Venmo -> **just use cash(!!!)**, maybe certain cryptos if you can get your friends to use them
* Discord -> Matrix, XMPP, IRC, various others
* Twitter, Facebook, etc. -> the Fediverse (Mastodon, Pleroma, etc.)
* reddit -> also the Fediverse (Lemmy, Kbin)

There's some more detailed discussion on how to go about this in the Projects
section above.

// TODO elaborate

## Philosophy

Computers are powerful.  If someone uses your software, they're trusting you in
a certain way.  That trust can be abused in various ways.  If you control the
software someone runs, you have some kind of power over them, and power can be
abused.  One of the better ways to ensure power cannot be misused is to give
the user the opportunity to take the power back if needed.  This reduces the
impact and risk of coercion.

// TODO elaborate

The consequence of this is that all software should be free software and all
information should be freely available.

### Concerns

Most large corporations are evil and want you to be dependent on them to ensure
their continued existence and profitability.  You should avoid them and take
steps to reduce your dependence on them.

It's unreasonable to expect to fully divorce ourselves from the surveillance
capitalist state without giving up some nice things and alienating ourselves
from our friends, but we can reduce the kinds of data we leak and work to
establish viable alternatives (kinda like dual power).

It's difficult to describe how quickly computers have dramatically all of our
lifestyles.  It's possible nothing in the history of humankind has caused as
dramatic of a shift as computers and modern telecommunications have.  For both
good and bad.  Trillions of dollars of wealth have been accumulated as a
result of this shift, and private corporations in control of our infrastructure
have power that rivals that of governments.  So trying to inoculate ourselves
against coercion by these actors I see as a moral imperative.

### Background Resources

These are pretty important to read/watch, and heavily informed my own opinion
on technology and our relationship to it.

* [Philosophy of the GNU Project](https://www.gnu.org/philosophy/philosophy.en.html) (all sections)
* [Revolution OS](https://en.wikipedia.org/wiki/Revolution_OS) (ask me if you don't know where to watch it free)
* [Permacomputing](http://viznut.fi/texts-en/permacomputing.html)

// TODO more

<!-- tz-insert-footer -->

