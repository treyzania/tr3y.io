# reddit: What is to be Done?

*Originally written 2023-06-09.  Revised 2023-06-11.*

I'm writing this because I've used reddit for about a decade now, having
registered in 2013.  I have some strong opinions about the direction it's gone
in recent years and I think that the current crisis should really have been
expected and is a long time coming.

(If you're already filled in on the background details and generally agree with
the sentiment, skip to the "Proposition" section.)

<!-- tz-insert-toc -->

## Background

On 18 April 2023,
[reddit announced](https://www.reddit.com/r/reddit/comments/12qwagm/an_update_regarding_reddits_api/)
that they would be making some substantial changes to their API.  It wasn't
exactly clear from this announcement what the impacts would be, but it became
clear on the 31 May, when the developer of the popular 3rd party
iOS reddit client Apollo
[announced](https://www.reddit.com/r/apolloapp/comments/13ws4w3/had_a_call_with_reddit_to_discuss_pricing_bad/)
that he had a call with a contact at reddit to discuss how the API pricing
changes would impact his app.  I'll just paste the excerpt:

> I'll cut to the chase: 50 million requests costs $12,000, a figure far more
> than I ever could have imagined.
>
> Apollo made 7 billion requests last month, which would put it at about 1.7
> million dollars per month, or **20 million US dollars per year.** Even if I only
> kept subscription users, the average Apollo user uses 344 requests per day,
> which would cost $2.50 per month, which is over double what the subscription
> currently costs, so I'd be in the red every month.
>
> I'm deeply disappointed in this price. Reddit iterated that the price would
> be A) reasonable and based in reality, and B) they would not operate like
> Twitter. Twitter's pricing was publicly ridiculed for its obscene price of
> $42,000 for 50 million tweets. Reddit's is still $12,000. **For reference, I
> pay Imgur (a site similar to Reddit in user base and media) $166 for the same
> 50 million API calls.**

Needless to say, this was devastating.

On 8 June 2023, there was waves of announcements from the developers of every
major 3rd-party client that they would be shutting down on 30 June, the day
before the API changes are scheduled to go into effect.

* [Apollo](https://www.reddit.com/r/apolloapp/comments/144f6xm/apollo_will_close_down_on_june_30th_reddits/)
* [reddit is fun](https://www.reddit.com/r/redditisfun/comments/144gmfq/rif_will_shut_down_on_june_30_2023_in_response_to/)
* [Sync](https://www.reddit.com/r/redditsync/comments/144jp3w/sync_will_shut_down_on_june_30_2023/)

There were also announcements from BaconReader, Relay, Infinity, and other
clients that the API changes would be affecting their users as well, of course.
The Apollo link there was especially illuminating since it covers a lot of the
communication Christian had with reddit over the course of the last week and a
half, including a recording of a call to keep the record straight.  It very
much seems like **reddit is acting in bad faith here** and that the changes are
very specifically to kill off and discredit 3rd-party apps.

The announcements [even made it onto TechCrunch](https://techcrunch.com/2023/06/01/developers-of-third-party-reddit-apps-fear-shutdown-because-of-api-pricing-changes/).

The necessity of these kinds of third party tools can't be understated.
They're absolutely essential for the platforms *unpaid, volunteer* moderators
to do their work in many cases.  The 1st-party reddit app is pretty widely
disliked.  It's slow, it's clunky, it's riddled with spyware, and its moderator
features are really lackluster.  They *say* they're working on improved mod
tools, but why can't they just let people use the tools they developed
themselves?

One example being
[the automation efforts by /r/AskHistorians](https://www.reddit.com/r/AskHistorians/comments/142w159/askhistorians_and_uncertainty_surrounding_the/).

### Commentary on 3rd party apps

Some people can argue that these 3rd party apps deserve what they're getting
because reddit gets to dictate the terms that people use their service.  They
say that when starting their businesses (which is honestly even a stretch of
the term, Apollo's developer only incorporated in order to handle the very
small fees (~$10/yr) to support some basic infrastructure and continued
development) these developers accepted a "platform risk".

But what people are really *outraged* about here is the break in the social
contract that had formed between reddit the company and reddit users.  The
reddit API had been stable for *years* and millions of people had come to rely
on it indirectly to access the site.  For many years there simply *wasn't* a
1st-party reddit app for mobile devices.  So numerous independent developers,
without any initial expectation of profit, decided to spend their own time
building apps, bots, and other tools that interact with the reddit API.  These
developers supported the reddit ecosystem, and *reddit* gets to benefit
strategically from it by making it easier to onboard users and provide a more
enriching experience, effectively for free.  It is reasonable for reddit to be
able to charge for high volume API access, but the prices they're stating do
not reflect a fair value for the service provided, as discussed in the earlier
quote.

I'd compare it much more to
[the Carterfone](https://www.usnews.com/opinion/blogs/economic-intelligence/2012/06/28/carterfone-case-showed-how-regulations-promote-competition).
The Carterfone triggered a case where the FCC ultimately decided that,
actually, it was valid for people to use their own 3rd-party equipment to
interact with a closed network (in this case, the AT&T telephone newtork) as
opposed to renting it from the operator, so long as it wasn't designed to
compromise/damage the network, and it was the responsibility of the network
operator to provide standards and information to ensure that this equipment
could be made safely.  This decision opened up the telephone network to all
kinds of use cases that were just impractical before, like dial-up internet
communication.

By this logic, I see it as similarly frustrating that we should be expected to
use the 1st-party reddit app(s).  Nobody can control which web browser you use
to access the web (although some try), so why should we be expected to do the
same for social platforms?  Platforms should be *expected* or even in some
cases *required* to build well-documented APIs for people to use.

Another possible argument reddit raises is that people using 3rd-party clients
impacts their ad revenue, since interacting via the API does not allow for
reddit to inject the same kinds of ads as they would on the web interface or
their 1st-party app.  I don't think this is a good argument.  If your business
model is *dependent* on forcing people to pay attention to things they'd prefer
not to, then you should find a different business model.  Which they have, with
reddit premium subscriptions, selling activity, and bulk data access deals.

## Why does this happen?

A lot of blame is being put directly onto Steve Huffman (aka "spez").  And he
does deserve blame.  But putting all of the blame on him is shortsighted.
People don't make decisions in isolation, people are a product of their
environment.  **We have to consider the *systems* that people operate in and
think about what forces influence us.**

Reddit was founded in 2005 with seed funding from Y Combinator, a startup
incubator with a long legacy.  It grew pretty fast, but an early turning point
was selling ownership of the company to Condé Nast in 2006 in exchange for a
large amount of early funding.  A powerful early figure within reddit was
[Aaron Swartz (rip)](https://en.wikipedia.org/wiki/Aaron_Swartz), who had a
great vision for the site in promoting free sharing of knowledge and
communication, hacktivism, and numerous other good values, but after the
acquisition he left (was forced out?) in 2007.  You could kinda say at this
point reddit's fate was already sealed, but it wasn't widely understood how the
startup world worked.  Social media platforms were still *new* and *exciting*
it wasn't clear what the end game would look like.

An interesting phenomenon that seems to have started happening around 2010 are
large internet platforms, social media included, that are exciting and popular
when they're young, but do a bait and switch once they become large and often
due to establishing a strong
[network effect](https://en.wikipedia.org/wiki/Network_effect) it can become
difficult for users to separate themselves from the platform and move to
another without cutting off ties.  It's happened numerous times.

* Digg did this
* Facebook did this
* Twitter did this (even before Elon Musk bought it and made it worse)
* Google did this
* Tumblr did this
* Imgur did this
* reddit is doing it now
* Discord seems to be starting it soon too
* and there's plenty of others that

Others have already done a lot of the work here in identifying why this keeps
happening to platforms we (used to) love.  Cory Doctorow wrote
[an article](https://www.wired.com/story/tiktok-platforms-cory-doctorow/)
earlier this year about the phenomenon and how it occurred to TikTok.
[(self-published article link)](https://pluralistic.net/2023/01/21/potemkin-ai/#hey-guys)

One important quote from this article that summarizes the entire issue:

> This is enshittification: Surpluses are first directed to users; then, once
> they're locked in, surpluses go to suppliers; then once they're locked in,
> the surplus is handed to shareholders and the platform becomes a useless pile
> of shit. From mobile app stores to Steam, from Facebook to Twitter, this is
> the enshittification lifecycle.

What [a commenter](https://news.ycombinator.com/item?id=35865071) in the
discussion for the Doctorow article on
[Hacker News](https://news.ycombinator.com/item?id=35863876) identified is that
Steam seems to be, on its whole, an exception to the Enshittification cycle.

> Personally I think the author doesn’t realize that Steam is an exception to
> this theory. It’s a private company that’s majority owned by a relatively
> idealistic founder. It’s also not really a social media company in the
> traditional sense, and it doesn’t really live and die on advertisements.

The key part here is that Steam isn't subjected to the normal forces of capital
markets that most startups are.  The leadership at Valve (owner of the Steam
platform), in particular Gabe Newell being the majority shareholder, can elect
to forego maximizing his own profits from ownership of the company.  Why does
he choose to do this?  We can speculate, but it's possible his time working at
Microsoft may have influenced his positions, and some kind of dedication to
*making good games* as opposed to *maximizing profit*.  Not all industries can
permit this kind of behavior.  A game company or a social media platform is a
very different kind of organization from, say, a supermarket chain.  Valve
*does* have controversies surrounding it, but it's a far cry from most
publicly-traded companies which go out of their way with shady business
practices, screwing over consumers and absusing laws to assert control over
their intellectual property.  Compare them and how they go about business to,
for example, [EA](https://en.wikipedia.org/wiki/Criticism_of_Electronic_Arts).
But enough about that, let's bring it back to reddit.

It's not really a secret that reddit has been gearing up for an initial public
offering (IPO) for some years.  What I think may have changed the game recently
is the US raising interest rates over the last year or so, causing capital to
pull back rapidly.  Their shareholders (which include [Fidelity](https://techcrunch.com/2023/06/01/fidelity-reddit-valuation/),
Tencent, and some others) want to know that their money is being put to use
properly and can actually provide a return.  This was always in the cards, but
the macroeconomic changes probably just accelerated it.  The way that reddit's
leadership has decided to answer this is, as many startups aim to, is by going
public and issuing stock on public markets.  That means that they have to shore
up revenue and cut costs, as they have been doing with these new API rules
going into effect and
[cutting 5% of its workforce](https://variety.com/2023/digital/news/reddit-layoffs-job-cuts-lowers-hiring-plans-1235635136/).

We know this is the direction they're going in because they've told us:
[one](https://www.cbsnews.com/news/reddit-ipo-filing-confidential-sec-wallstreetbets/)
[two](https://www.protocol.com/bulletins/reddit-ipo)

Once this happens, there is no path for reddit to turn around and start being
nice to its users again.  As described in the Doctorow article above, they've
spent as much time as their investors would allow being nice to the users, now
it's time to actually start making them money.

**CEOs of all companies have
[fiduciary duty](https://www.nolo.com/legal-encyclopedia/fiduciary-responsibility-corporations.html)
to their shareholders.**  This deprives them of some capacity for independent
action.  They cannot do anything well-intentioned if it damages shareholder
value  We cannot *entirely* place the blame on Steve Huffman for the direction
that reddit has been going in recently, since he's a conduit for the will and
interest of reddit's board and shareholders who (unlike Gabe Newell) make it
their whole business to maximize profits.

**This will always happen as long as social media is designed around the
startup growth model.**  We must find an alternative.

## Proposition

Many people have proposed alternative sites to reddit.  What I consider under
this umbrellas is the core concept of threads being based around a post (either
a link, text, or both) with comments forming a tree of replies.  Secondarily,
the organization of threads into discussion boards like subreddits, typically
with their own community moderators.

I've already linked one here, Hacker News.  HN is a bit of a weird niche, run
as a small discussion forum by Y Combinator.

There's also [Lobeste.rs](https://lobste.rs/), which is a similar vibe and
scale as HN, and works on a basis that you have to be invited to be able to
post, or have an application manually vouched for.

There's a few others that I'm stealing from
[this post](https://www.reddit.com/r/RedditAlternatives/comments/14539ql/reddit_alternatives_you_should_use_tldr/)
on [/r/RedditAlternatives](https://www.reddit.com/r/RedditAlternatives/):

* [Tildes](https://tildes.net)
* [Squabbles](https://squabbles.io) (nonlibre backend, red flag)
* [Raddleme](https://raddle.me)

These are all neat projects that should be considered.  I haven't thoroughly
investigated them myself, but they are clean and each have their own merits.

But what these all have in common is that they're *platforms*.  The risk here
is that *success is dangerous*.  If any of these sites becomes successful if a
[Digg-like exodus](https://en.wikipedia.org/wiki/Digg#Digg_v4) from reddit and
leads to rapid organic growth, then it's painting itself as a target for VCs.
We can trust that the people operating these platforms won't sell out, but the
risk will always exist, even if they're economically sustainable without
outside investment.  Any day a VC might decide to make an offer they can't
refuse.  If we jump ship to a VC-backed startup then it's going to be a
situation of *meet the new boss, same as the old boss*.

This has happened before in differing forms throughout history.  Whenever
there's a strong system of social ties keeping a community together, finance
capital tends to end up to inserting itself into the environment and disrupting
it.  One obvious comparison being the
[enclosure](https://en.wikipedia.org/wiki/Enclosure) of
[the Scottish clan system](https://www.scotclans.com/pages/the-clan-system) by
the English.

Whatever ends up being the successor to reddit has to find some way to avoid
this fate.  We *must* have a new model if we want to avoid having exactly the
same situation happen again in another 10-15 years.

### Where to go?

Alternative models for social media have been around for a while.  The concept
I'm referring to actually goes back far, *very* far.

[Usenet](https://en.wikipedia.org/wiki/Usenet) was conceived of in 1979 as a
way for users to share posts (or "news") between each other, relying on a wide
network of independently operated servers that exchanged data opportunistically
over the weak dial-up links of the day. While most users had home servers,
their posts were replicated across the network.  Usenet fell out of favor in
the late 90s as broadband internet access started to become more popular and
web-based forums that supported more features started to become more popular
for discussions.

The modern conception of the "federated universe", or
"[Fediverse](https://en.wikipedia.org/wiki/Fediverse)", started in 2008, with
[identi.ca](https://en.wikipedia.org/wiki/Identi.ca).  It was never
particularly widely used, but a newer iteration came onto the scene with
[Mastodon](https://en.wikipedia.org/wiki/Mastodon), which you may have already
heard about as a result of the current Twitter drama and slow disapora.  Anyone
can download the software, for free, and start their own "instance" of it.
These instances host user accounts, and user posts are shared between instances
freely.  Instance owners can set their own content moderation policies, similar
to how moderators of subreddits do, but with more freedom on how to implement
them.  You can think of your instance like an email provider, but operated by
trusted community members, and with more transparency into operations.

Some kind of decentralization *and* interoperability between instances I
consider to be essential because it makes it less likely for users to
concentrate in a single place without a way to break apart the network effect,
so instead of instances *competing* for users, the value of the network *as a
whole* become stronger when any one instance does.  Federation is a model that
can and does function similarly to what people are already used to with
centralized platforms like reddit, and so the friction to migrate users should
be modest.

It's not clear to me what exactly about Mastodon made it reach a critical mass
of users, but its undeniable at this point that it's going well.  It's well
beyond the point of being just the core developer(s) and their close friends,
it's an ecosystem greater than itself.  Implementing a microblogging system
like Twitter is simple and it's a model that people are already comfortable
with.  There's similar software packages like Pleroma that can freely
interoperate with Mastodon instances.  All of these software packages have the
same kinds of APIs to exchange posts *with different servers* running
compatible software, similar to how Usenet worked.  Today, there's thousands
of different Mastodon instances, with a total number of registrations somewhere
in the range of 1.5 million.

It's not all sunshine and roses.  There has been troubles with the Fediverse.
Some of them are technical, some of them are
[more cultural](https://www.theguardian.com/technology/2023/apr/18/mastodon-users-twitter-elon-musk-social-media).

* Not everyone agrees on how content should be moderated, since each instance gets to define their own policies for users.  This has led to a small amount of sectionalism.
* Some people might want new features (quote replies being a big one) but others may dislike how it could change the social dynamics.
* Sometimes large instances may run into funding issues.
* Some people have proposed adding a more structured content recommendation algorithm, while some reject it for going against the values of the project.  Some instances *do* elect to add these features to a modified version of the software they run on their servers, which is part of the benefits of choice in theory.

These are growing pains that many movements have had to deal with, and I
believe that they can be addressed if the people pushing these projects forward
are dedicated enough and willing to work together.

The software powering Mastodon instances is free and open source, published
under a strong copyleft license that helps eliminate the risk of capture by bad
actors that wish to extract value from the network.  There is a nonprofit owned
by the founder of the project, Eugen Rochko, and serves as a custodian of the
software.  The actual ActivityPub protocol that defines the APIs between
servers is managed by the W3C, the consortium that manages many web standards.
While there is a flagship Mastodon instance,
[mastodon.social](https://mastodon.social), Eugen himself has stated he sees it
like an "airport".  By default, people are often directed to join there, then
after interacting people on other instances for a while, they can make a new
account on another instance with a culture based more around their interests.
There's also sites like [joinmastodon.org](https://joinmastodon.org/), which
help users find instances upfront based on their interests.  Development of the
software is funded via Patreon donations, which is also what a lot of instances
use for sustaining themselves.

Mastodon is still very much immature, but it has a lot of potential and it has
cleared many of the early bars that would be in the way of success.

**So how does this relate back to reddit?**  There's a couple of attempts to
implement a reddit clone on top of the same ActivityPub protocol that Mastodon
uses.

The two that I'm aware of right now are [Kbin](https://kbin.pub/en) and
[Lemmy](https://join-lemmy.org/).  Right now the ecosystems around these are
*very small*, but there are already a number of instances.  Especially given
the last week or two, I expect that some dedicated subset of people will feel
strongly enough to refocus their development efforts on these projects.  They
will have to go through many of the same growing pains that Mastodon did and
still does, but we kinda understand how this works now.  I don't have a
tremendous amount to say about these at feature level, because they're reddit
clones, if you're reading this you should already have a good idea of what
they do.  The main difference is that users with accounts on instances can
subscribe to forums (term I'm going to use instead of subreddits) on other
instances *from their own instance* making the boundaries between instances
thin.

Another possible concern with Lemmy is that the lead developer and owner of the
flagship instance (lemmy.ml) has some *concerning* politics that make me start
to believe in horseshoe theory.  I don't think this is an existential issue for
the software, since *anyone* can fork it and do their own thing with it.  It
doesn't *seem* like the developer's politics have had a strong impact on the
software as it stands *today*, but I don't know how that could change in the
future.  After all, we do still read and build on HP Lovecraft's work.
**Fortunately**, since it *is* free and open source just as Mastodon is, if
the direction the original developer(s) want to take the software in is at odds
with the community, they can fork it or any older version and take its own
direction.  There are forks of Mastodon, like glitch-social, ecko, Megalodon,
which are all of course compatible with upstream Mastodon instances, as would a
fork of Lemmy be with upstream instances and Kbin instances.

I really can't say what the best direction to take between these two projects
is.  Lemmy has a head start but the reputation issues might be a problem if the
community can't assert its own direction over its founder.  I am fully in favor
of establishing an independent fork if the community decides the ideal path to
take.  Kbin may have a slightly differentset or have a more accessible UI but
it's more of a departure from reddit and is based on PHP, which may be an issue
in the long run, but that might be fine.

I've also seen someone build [a Lemmy API proxy](https://feddit.de/post/768790)
that has the same API as reddit, so it's possible (forks of) the 3rd-party
clients that reddit today could be easily retooled to just plug into these.
The fact that these projects *almost* technically have fully-featured clients
on Android and iOS, *that are already installed on many thousands or millions
of devices* could be a secret weapon that makes adoption of these new protocols
easy.  It may become a standard part of any instance to run a reddit-like API
proxy to support these retooled apps.

**Update:** I have been informed that RedReader is planning to support use of
Lemmy instances in parallel, and possibly others.
[(official announcement)](https://old.reddit.com/r/RedReader/comments/145du4j/update_4_redreader_granted_noncommercial/)

If you want second opinions, here's two articles I found in the process of
writing this article:

* [Don't tell people "it's easy"](https://privacy.thenexus.today/dont-tell-people-its-easy-draft/)
* [On Reddit and Its Federated Alternatives](https://www.jayeless.net/2023/06/on-reddit-and-alternatives.html)

### How do we do it?

I think we have a good opportunity for people to come together here for
organized action.  Moderators of popular subreddits have a level of legitimacy
here that could be leveraged in order to unify people, if they so chose.  I am
not a moderator of any active subreddit so I don't really have the grounds to
tell moderators what they *ought* to do, but I have operated other kinds of
internet communities and I've studied these kinds of crises in online
communities and the diaspora that tends to follow, so I feel that *somebody*
might find these prescriptions valuable.

The moderators should settle on one or two software packages to invest their
time in, probably either Kbin or Lemmy, and launch a set of instances based
around general topics, and serve as administrators.  It would be self-defeating
to fit everyone on a single instance, but people *do* tend to
[cluster around particular subreddits](https://lmcinnes.github.io/subreddit_mapping/),
which makes the federated model natural and federation makes the boundaries
between instances pretty thin anyways.

The endgame that I would like to see is for the moderation teams of major
subreddits to form nonprofit cooperatives to operate instances and aid in
funding development of the software ecosystem.  A common criticism of the
Fediverse today is that people who run instances (since the vast majority are
run by individual people) can just decide to randomly shut down your instance
and you'd be screwed and without a data backup.  This has basically never
happened, on the rare occasion that an instance does shut down, the
administrators almost always give substantial advance notice, but having a
formal legal cooperative structure would ameliorate these concerns and provide 
stronger continuity of service.  The right bylaw structure can aid in this goal
and reduce risk of hostile takeover, and prevent cases like what happened to
[Freenode](https://en.wikipedia.org/wiki/Freenode) outright.

By being *owned* by moderators means that a charismatic VC would have to get
buy-in from a supermajority of partners in order to make the shift from
nonprofit and community-owned towards a for-profit model that takes outside
investment capital, as well as dealing with the tax/legal hurdles that would be
involved with that.

This should be a more common legal framework for operating Fediverse instances.
It should extend to more than just reddit clones.  I'm not a lawyer, I don't
know how this should be set up, but I *know* it's possible to do at scale.

Something to explore could be a dues-based membership structure, like a social
club.  If you're a dues-paying member you could have some *legally binding
vote* on the direction of your community.  I would expect it to be some kind of
bicameral structure to avoid situations of tyranny of the majority, but it
could be a way for nominate new moderators/admins and recall them like
delegates.  This would make these more like 501(c)(7) organizations instead of
the typical 501(c)(3) type of nonprofit, to put it into US tax code concepts.
It would also reduce risk of rogue administrators/moderators (a common
critique) from 

[This HN comment](https://news.ycombinator.com/item?id=36284595) identifies a
phenomenon in-line with this idea, since people active in a social space over
long periods of time feel ownership of the space:

> Plus, in my experience , users will bitterly resist any changes at all
> because the site doesn’t belong to management, it belongs to them. It’s their
> space. Changing anything is like someone’s snuck into their house at night
> and remodelled their lounge.

Here's the groupings that I see might make sense, roughly in the order I think
they should be started:

* programming and technology
    * programming language communities
    * around companies: apple, google, microsoft
    * theory topics
* general discussion
    * AskReddit -> AskFedi
    * all the other Ask*-like subreddits
    * /r/IAmA, etc.
    * also probably include meme subreddits in here too
    * /r/funny, /r/aww, /r/pets, etc.
    * might be too broad for moderators to come to aggreement
* movies and TV
    * general discussion
    * franchise-specific
    * more memes
* music, art, maybe books?
* sports
    * possible risk that people might hate each others' teams too much
    * possible risk that if the instances are too focused on particular sports, it puts them at risk of being targetted by a major corporation behind it, like the NFL, NBA, MLB, FIFA, etc.
* other smaller more tightly-knit communities may also start their own independent instances not under a larger umbrella

(Someone should make a git repository with a machine-readable mapping of
subreddits to alternatives.)

It would make sense to split this up by language too, since something reddit
kinda struggles with is being too Anglo-centric.  I don't think it would make
sense to have dedicated political instances, since Fedi is already fairly
politically charged and it might, unfortunately, lead to too much discord that
it breaks down the instance.

There is still a "market" for independent forums to operate and be self
sustainable, there still exists forums running on phpBB or something similar,
for specific topics like cars, bodybuilding, trainspotting, etc.  These are the
kinds of forums that reddit (and more recently, Discord) kinda killed, but we
can bring them back with more modern software architecture and the right
motivation.  If we're successful, we could use a similar approach to this to
supplant Discord, too, if there is ever a suitable stack to do it with.

Instead of building a moat using a network effect out of the userbase, **we
should build a shield by using a network effect in the form of standards and
implementations**.  This makes it harder for a would-be captor as they would
have to compete on multiple fronts simultaneously.

There's *so many* good ideas that just haven't been explored yet.

Here's few I came up with:

* automatic post mirroring, scraping links from subreddits to corresponding feeds
* "log in with reddit", although this would have to be fairly clever as the standard OAuth2 based login scheme would just be banned fairly quickly
  * having some "verified migrant" status could be appreciated
* subscription imports, where users are automatically prompted to subscribe to "successor" forums based on their reddit account post history (or direct import)
* periodic archives exports of public content, distributed via bittorrent, to be used in the event the instance does have to shut down and identify if moderators misbehave
  * users should be able to opt-out of these exports

I don't see how these could be reliably blocked without *really* cranking down
on API access, since they wouldn't necessarily require very many API calls and
could be done by scraping the 1st-party web interface.

Something that I would like to re-emphasize about free and open source software
is that the people who build the software actually use it and anyone is able to
make improvements that we can all benefit from.  There are issues with current
implementations of these ideas, but people are aware of them.  And as the
ecosystem becomes more popular, more people will step up to improve things.  On
another corporate-owned social media platform it wouldn't work like this and
you'd be subject to whatever agenda they have.

There are good things about how the internet used to work, and there are good
things about how it works now.  We can't return to the past, but we can learn
from it and can try to make the future better.

## Addendum

### Particulars of the API costs

There are actually still going to be ways to use the APIs for free that have
been announced.  It's not clear to me exactly if this is a rollback of the
initial plans or what the scope of it is.

> * 100 queries per minute per OAuth client id if you are using OAuth authentication and 10 queries per minute if you are not using OAuth authentication.
>
> * Today, over 90% of apps fall into this category and can continue to access the Data API for free.
>
> [...]
>
> * Accessibility - We want everyone to be able to use Reddit. As a result, non-commercial, accessibility-focused apps and tools will continue to have free access. We’re working with apps like RedReader and Dystopia and a few others to ensure they can continue to access the Data API.

([from the spez AMA](https://www.reddit.com/r/reddit/comments/145bram/addressing_the_community_about_changes_to_our_api/))

This still rules out apps like all the ones listed in the introduction section,
although it *may* allow some special tooling.  How exceptions for nonprofits
and accessibility apps will work hasn't been announced yet.  There's no
explanation where that 90% number comes from and it *certainly* doesn't refer
to *users*.

### Old reddit interface

A few years ago, reddit completely changed its user interface to unify it with
the 1st-party app, which was new at the time.  It was pretty widely disliked
along with the app, for many of the same reasons, because it was literally the
same codebase (being a big bloated React.js app).

Reddit knew this would be controversial, so provided the "old" interface at
[old.reddit.com](https://old.reddit.com), but never clarified if this would be
here to stay permanently.  It's easier to block ads on this older version, it
doesn't have as many capabilities for user tracking, new features are typically
only being added to the new version, and some particulars with the updated
markdown rendering are broken on the old version.  So it seems from their
actions that they definitely see it as being a second-class citizen and we
certainly can't rely on it.  The new UI is also far less accessible for users
that need assistive technologies (see
[/r/blind's thread on it](https://www.reddit.com/r/Blind/comments/13zr8h2/reddits_recently_announced_api_changes_and_the/)).

Many users have stated that if "old reddit" is ever removed and 3rd-party apps
are still impractical to use that they will simply stop using reddit for good.

### What is an API?

To put it simply, an application programming interface (API) is any kind of
specification for developers to use to write software that interacts with some
other software system.  Typically, this takes the form of a service running on
the internet that developers can make calls out to to interact with.

In reddit's case, the API provided methods to interact with the platform
programmatically in the same ways that a user would through the web interface:

* "What are all the posts on the front page?
* "What are the comments in this thread?"
* "Upvote/downvote this post."
* "Post a comment in reply to this post/comment."
* "Post a new thread with a link to a site."
* "Search for posts with some keywords."
* and several others for most kinds of user interactions

These functions are called and returned data in a *machine-readable* format
(JSON), which make it trivial for developers to integrate them into other
systems.

But often, if a platform company has a 1st-party app that users are expected to
use, even if they have a public API, they may also have private undocumented
APIs that only they themselves have the knowledge to use.  They do this for
various reasons, it's almost expected that this happens.  You can file the
justification under "competitive advantage" or "platform control" or something,
but it still frustrates developers who want to work with a platform to
effectively *do free labor* for them.

Some may argue that open APIs open the doors for abuse, but this is a weak
argument.  Reddit has the capacity for throttling excessive API usage spam and
*already* has robust spam prevention mechanisms.  Scraping the site to train
LLMs as others have already suggested is already doable through the web
interfaces, and the site is so huge that AI companies like OpenAI and DeepMind
are better off signing bulk data deals.  This would mutually beneficial to both
compared to going through the API which would be much slower and doesn't deal
with bulk data as well.

### Distributed peer-to-peer social media protocols

There are a few projects that are trying to build fully distributed p2p social
platforms.  Jack Dorsey from Twitter history funded the development of
[Bluesky](https://bsky.app/).  There's a number of crypto-based projects that
are trying it out as well.  [Nostr](https://nostr.com/) is a protocol that many
have put forward to build Twitter-like clones on top of, and it stands to
reason that a reddit clone could be built as well.  They have some valid
criticisms of the Fediverse model of federated servers, but most of the issues
put forward are solvable either though technical means or more established
social organization like I've described above.

The other issues with these protocols is that the approach is a much larger
departure from what users are already familiar with with standard websites.
For something like social media, most users will have their eyes glaze over as
soon as you start talking about private keys.  It should not be possible to
have catastrophic and irrevocable account takeover, and these protocols really
don't have any answers to that kind of account yet, without involving *more*
kinds of blockchain words.  The kinds of technical properties you get from
involving blockchains are really *really* overkill when you already have some
kind of social ecosystem to operate in.

Also, many of the crypto-based "web3" projects *also* have VC funding, just as
reddit did.  They often are much less decentralized as they claim to be, or in
practice are simply impractical to *be* decentralized.  They *have plans to*
eliminate these central services that they operate, but given that they're VC
funded the will never take actions that are entirely in service of the users
since they have to make an ROI.  If it's not through some directly extractive
means of charging for services, it'll be by issuing some illegal crypto token.

Maybe there will be a successful project one in 5-10 years, but the time for
action is now.

### Data deletion and privacy

Another common critique of Fediverse protocols is around data privacy.  Since
posts are shared with other instances, when you delete a post, a message is
sent out to the other servers directing them to delete it.  But it is possible
that nefarious people can run forks of the software that simple disregards
these deletion messages.

This has happened once or twice, but if they do it publicly then administrators
typically quickly "de-federate" from that instance, and refuse to forward new
posts to them.  Most software also supports a "local-only" privacy setting,
where their posts are public but *only* to local registered users on their own
instance, and won't be relayed to others.  I don't know if this functionality
exists in Kbin or Lemmy yet, but it should be easy to implement.  As I
described before, with community ownership of instances, it can be harder for
operators to lie to us about what's actually being deleted.

But standard reasoning should apply.  Public posts on reddit *absolutely* get
archived by people, both individuals, corporations, and governments.  There's
no reason to assume that becuase you deleted it from reddit that all copies of
it don't still exist.

Another common question is about DM privacy.  DMs are not end-to-end encrypted.
But neither are they on reddit or Twitter either.  Administrators can *in
theory* see the contents of messages, but only by poking around manually in the
database.  There are some people working on an end-to-end encryption proposal,
but if you actually really need privacy you should be using a dedicated
messenger for that like Signal, Matrix, or some others.

## Updates

*(Section added starting 2023-06-18.)*

There's been a whole lot of discussion and people coming to many of the same
conclusions as I have independently of me.

One thing that was posted on Hacker News is this
[Reddit Migration Directory](https://redditmigration.com/).  There was another
one similar to this but I can't find it now.

This is just a slice of subreddits, but it is quite a few already.  It's not
quite as diffused as I had originally envisioned and there's some fighting
about federation policies, but it's a start.  Someone pointed out that the
groups of people outraged by these changes may be a vocal minority (hard to say
for sure since only reddit themselves have these statistics), but they do
constitute much of the *content creators* on the site, the people that initiate
and participate in discussions that drive the engagement reddit uses to make
money through ad impressions.  If these users migrate (many of whom are), then
the content that reddit relies on will starve.

There's also been some accusations that reddit
[may be undeleting user posts](https://news.ycombinator.com/item?id=36354850).
It's unclear if this is intentional or is a side effect of private subreddits
impacting how user self-deletion works, or is some other database sync bug.  It
would not be out-of-character at this point for it to be intentional, and if so
then this could constitute a massive GDPR violation.  We'll have to see.

<hr/>

*Will the last person to leave reddit please turn out the lights?*

<!-- tz-insert-footer -->

<span style="color: #999">
I'm using the lowercase **r** here when writing "reddit" because that's how it
used to be written, back in the old days.
</span>

