# Mastodon and the Fediverse have already won

*Revised 2023-10-02.*

Twitter is dying, everyone knows this.  There's a bunch of ways this is true,
but this article isn't meant to be about that.  This article is meant to be
about the many projects that are trying to be a successor to the niche is holds
in the social media space.  I will assume readers already have some working
understanding of how the Fediverse works, like what an instance is.  If you want
some more background knowlege, here's some resources:

* [A beginner’s guide to Mastodon, the open source Twitter alternative - Techcrunch](https://techcrunch.com/2023/07/24/what-is-mastodon/)
* [Join Mastodon](https://joinmastodon.org/)
* more tangentially related: [God Did the World a Favor by Destroying Twitter](https://www.wired.com/story/god-did-us-a-favor-by-destroying-twitter/)

See also [reddit: What is to be done?](/articles/tech/reddit.html).

<!-- tz-insert-toc -->

## Competition

Off the top of my head there's a handful of projects that are in some way trying
to be successors to Twitter.

* the Fediverse, chiefly Mastodon, but also related projects based around ActivityPub
* Bluesky, as funded by former Twitter chief Jack Dorsey
* Threads, compatible with ActivityPub but really meant to be a silo controlled by Facebook (or, as they want you to think of them "Meta")
* a bunch of crypto projects that are going nowhere, including but not limited to
  * Nostr in the way most are treating it
  * Lens
  * others I can't remember because [they're stupid and pointless](https://makemake.site/post/blockchain-bad)

(please suggest others so I can include them for completeness)

The key lesson for why "mainstream social media platforms" have existed and
stayed dominant for so long is that they heavily leverage network effects and
low interest rates letting them offer unfairly-good deals for a long time.  But
as others have noticed this can't really last forever, eventually it comes time
to pay the piper.  Author and technologist Cory Doctorow named this phenomenon
[enshittification](https://pluralistic.net/2023/01/21/potemkin-ai/#hey-guys).

The point I would like to make here is that Mastodon (and the Fediverse broadly)
has already won the race to replace Twitter, most of the users (including
current Fedi users) just haven't realized it yet.  Not in the sense that,
somehow, there's a hundred million Fedi users that nobody noticed.  But in the
sense that, assuming the current trends continue, Fedi will *eventually* displace
all existing competitors.  There's two prongs to my argument here, the first
point is based on development and funding models of these protocols constraining
the ways they can grow, and the second point is based on the nature of network
effects in social media and the nature of the users that have already migrated.

## Social legitimacy

The common theme with the issues with the other supposed competitors is over
the *legitimacy* of their claim to be the successor.  I'd like to link
[Vitalik's article](https://vitalik.ca/general/2021/03/23/legitimacy.html) on
this, in which he articulates what I mean by this better than I could.  He then
goes and talks about NFTs which I don't really like, but that's more of a end
thought that I don't think takes away from the core argument.

I want to discuss some more focused points particular to some of the competitors
that are coming.

### Corporate challengers

The big ones trying to pick up the reigns are Bluesky and Threads.

Bluesky is a vanity project that was funded personally by Jack Dorsey, based
around a more generic "AT Protocol".  There's a lot of intersting design
decisions they made, decisions that people have already critiqued elsewhere.
But Bluesky is kinda directionless at the moment.  It does have some active
users, reporting 1.2M, but it still has a waitlist for registrations and is
otherwise invite-only.  It's not really clear what the timeline for opening it
up further is.  And it's reported that Jack even
[deleted his own Bluesky account](https://www.reddit.com/r/BlueskySocial/comments/16l8cl7/why_dorsey_deleted_his_bsky_account/)
inexplicably.

Threads is Facebook's answer to Fedi.  It bootstrapped itself by essentially
being built out of Instagram infrastructure like a chestburster from Alien,
borrowing all its userbase and making it easy onboards a Threads profile from
Instagram.  This gave it a huuuuge initial bonus, claiming 200M users.  Of
course this led people to say that "Meta did it!  They're going to replace
Twitter!", whether that's a good or a bad thing in their worldview.

But it seems that this also dropped precipitously, with
[some estimates](https://techcrunch.com/2023/08/31/threads-web-launch-did-little-to-bring-in-more-users-new-data-suggests/)
suggesting it's sitting at somewhere around 1M daily active users.  That sounds
like a shitload, but remember, this is a Facebook product.  They're the company
that told their investors that they had burned through the set of possible new
users and that there isn't much of anyone *left* to turn into a new active user.
So it's a pretty big flop if you put it into perspective of the power they
theoretically have.

As an aside, a large goal for Threads was ActivityPub support, allowing interop
with Fediverse instances.  This hasn't happened as of the time of writing (as
far as I'm aware), but it wouldn't be likely to change much in the eyes of
people trying to pick between it and Fedi more broadly, since everyone that
would be considering it knows it's still just Facebook.  Fedi users aren't
Facebook's target audience.

They're mostly still just curiosities at this point.  Even the people I've
talked to that are jumping onto Bluesky aren't wholly sold on it.  Threads
carries a very astroturfed and advertizer-friendly feel to it.  I have yet to
meet anyone that actually *likes* using Threads.  If *you* do, then I'd love to
talk to you.

<center><iframe src="https://mastodon.coffee/@rob11563/111100822028813123/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="600" allowfullscreen="allowfullscreen"></iframe><script src="https://mastodon.coffee/embed.js" async="async"></script></center>

But like, a more important question we should be asking is "why should people
trust these platforms anyways?".  People can kinda tell when they're being taken
for a ride, even if they can't articulate it, and these platforms don't really
do anything to address the systemic problems that people have with Twitter.  I'm
confident that if we swapped places and had a feature-mature Fediverse with as
many users that Twitter had at its height going against even a *mature* Twitter
with all of the features it has now (but starting from 0 users), it would
struggle to ever get off the ground.  There's no *feature* a centralized social
platform can have that a decentralized social network couldn't possibly provide
in some way, and a platform that has to make money will always have to extract
it from its users once the VC money runs out.

It's also possible that Facebook doesn't really care that much about Threads and
that it was more of a corporate show for investors rather than a serious
strategy. They have a shitload of money they can throw into a project in the
event it works out, without the expectation that it does.  But if they don't
make a move then investors will complain that they're missing the opportunity on
gobbling up users in the exodus from Twitter imploding.

### Moonshots

For other reasons that I've already articulated in the reddit article, I and
others think that it's pretty important that whatever successor to Twitter there
is is decentralized.  But people have lots of different visions for this.

Most of the crypto-based social media protocols are accepting VC funding.  So
many of them don't even have organic user behavior, with Lens literally just
using bots to be able to say to investors "this is what it will look like when
it takes off".  The core development team will not be likely to give up control
over the project to turn it over to community development any time soon, and so
there won't *be* much community development, outside of potentially some grants
coming from the VC money to astroturf an ecosystem into existence.

Since they're backed by VCs, there will *always* be a desire to extract a
revenue stream to pay back the investors.  The enshittification cycle will
manifest in some way, sooner or later.

On the other hand, **Mastodon development is driven entirely
[through donations](https://www.patreon.com/mastodon)**.  Currently, the nonprofit
that manages development and the flagship instance receives $27k/mo just throgh
Patreon.  Mastodon doesn't *need* to have exponential growth.  It doesn't *need*
to find a way to monetize its users through the core protocol.  It doesn't
*need* to find ways to lock in its users like corporate-owned platforms do.  It
doesn't *need* to be advertiser-friendly and get corporate customers onboard.
The costs of operating the network are borne by its own (power) users directly,
and development model is already self-sustaining.

Additionally, in most cases, these crypto social media projects associated with
some scammy crypto coin that is going to push away normal users that don't have
the crypto brain poison, which means they're also standing at a disadvantage.

There's also the simple UX issues of these up-and-comers too.  Normal people *do
not* want to deal with private keys, especially for something as trivial as
social media.  You can convince them to care if it's something more serious like
keeping their crypto funds safe, but usually that's best done through the
abstraction of a hardware wallet.  A completely unacceptable UX burden for
something like social media.  And all the negative issues that come out of it
(like usually losing access to your account if you lose your private key, as it
is in Nostr) also are unacceptable for most users.  Bluesky has a much better
approach in this area.

## Power users

While network effects can manifest in many different contexts, in social media
they take an interesting form.  The
[1% rule or 1-9-90 rule](https://en.wikipedia.org/wiki/1%25_rule) is the idea
that 1% of users in a mature social media space actively produce new content, 9%
engage with it passively, and 90% are lurkers that only consume content.  In
reality, it's more like a power law distribution rather than 3 clean buckets,
but it's a rule of thumb after all.

A related idea in network theory is the idea of a
[small-world network](https://en.wikipedia.org/wiki/Small-world_network), which
is characterized by most nodes only having a few connections but a few nodes
having many connections.  These highly connected nodes are the people that tend
to be the ones that produce the majority of the content that all of the rest of
the people (nodes) interact with.  You may be more familiar with the observation
that everyone in the world only has "6 degrees of separation" from anyone else
in the world, which relies on that most social networks take this form.

These creators that make a platform worth it for these lurkers are also the real
power users that are especially sensitive to changes with the platform, making
the risks of pissing them off that much greater.  This is why the response to
the reddit situation was so organized and kicked off a healthy community in the
"threadiverse" (the subset of the wider Fediverse based on reddit-like
discussion boards).  I'm using the term "creator" as a catchall term for very
active users that many other users pay attention to.

A conclusion we can make from these observations is that **if you win over the
1% (or the 9%), the 90% of lurkers are likely to follow**.

This does appear to be happening.  New groups of users are joining, and they're
producing lots of content and activity, especially since Musk's takeover of
Twitter in November 2022 giving people a strong reason.  Journalists post
*a lot* of content and are very active about their topics of interest.  The
instance [newsie.social](http://newsie.social/) was created specifically
focusing on supporting journalists, and currently has >2k MAU (a lot for a
single instance focused around one topic) and has >796k posts.  Another group of
content producers is game developers on
[mastodon.gamedev.place](https://mastodon.gamedev.place/), with >8k MAU.  I've
also seen a number of webcomic artists around in various places.

Seriously, go check out
[JoinMastodon's server list](https://joinmastodon.org/servers).  There's a ton
of new instances that weren't here a year ago that are growing and active.
There's even pretty great active instances focused on people's own cities, like
[better.boston](https://better.boston/) for my own city and
[sfba.social](https://sfba.social/) for the SF Bay Area, among many others in
the US and Europe.

While this is mostly conjecture and I haven't empirically studied the flows of
users, if you're an active Fedi user you might agree with the reasoning above.

Another observation that I made (all the way back in 2019) was that what sets
Fedi apart from most of the rest of the alternative social media projects was
that **its users don't talk about Fedi itself that much**.  They talk about
whatever other random things interest them a lot more than they talk about Fedi.
If you look at a lot of other projects, their users (when they're actually
organic users) mainly just talk about how cool they think the project is.  Nostr
is pretty bad about this in my experience with this, as its users mainly just
talk about how cool Nostr is.

Since Fedi is still experiencing an influx of new creators we can't extrapolate
easily to compare the sizes of the *engaged* userbase there is.  Since social
networks are small world networks we can't just compare plain MAU numbers to
compare the strengths of the network effects it has against Twitter.  And since
Fedi isn't mature yet, it's still experiencing a large influx of the most active
kinds of users, we can't just use the 90-9-1 heuristic.

Another interesting phenomenon that has not happened anywhere else is
*governments* running their own Mastodon instances, like
[EU Voice](https://social.network.europa.eu/).
As it turns out, they don't really like being dependent on the social media
platforms based in another country.  Having a free and open network where they
can maintain an official feed for the many different institutions within them is
pretty desirable, without the meddling that comes from privately owned foreign
actors.

## Moving Forward

There's still a lot of issues with the user experience of Mastodon, we know
this.  These will improve and the avenues for getting user feedback are much
more transparent than *any* corporate social media site.  A big one people want
is a better recommendation algorithm to improve the new user experience.

People forget how functionally shitty Twitter was in its early days, it's unfair
to compare the UX of a massive mature social platform with millions of
person-hours of engineering time against a foss project with like 2 full time
developers.

<center><iframe src="https://mastodon.social/@bastianallgeier/109308360571103988/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="600" allowfullscreen="allowfullscreen"></iframe><script src="https://mastodon.social/embed.js" async="async"></script></center>

We can complain all day about little features Mastodon needs and why it not
having those means it will never be successful.  But, somehow, people don't
consider that *those features can (and will, if they're actually desirable) be
added*.

Some people, in particular the pro-crypto-social-media ones, argue that because
Mastodon isn't fully distributed that it will trend towards centralization over
time.  This is a really funny critique because some of the crypto social media
protocols are themselves centralizing around for-profit-owned indexers and other
central providers.  Bluesky itself even implicitly depends on an indexer that
maintains a large economy of scale.

But if you look at [FediDb](https://fedidb.org/), the opposite phenomenon seems
to be happening with Fedi.  Since each instance is entirely independent and
there isn't any need to rely on "supplementary" third party infrastructure, and
both users and administrators *prefer* diffusing out into separate instances to
avoid the moderation concerns of many different users with different
expectations in the same space.  The total population of the small/medium-sized
instances is growing proportionally more quickly than that of the larger ones.

## Conclusion

This isn't meant to be a thorough analysis.  It's really hard to concretely
study social phenomenon, so most of this is conjecture.  But I hope that I
provided some interesting propositions that you may not have have considered
before if you aren't actively paying attention to these projects.

To be fair to Nostr, it wasn't really *meant* to be used as a social media
platform at first.  It was a design consideration, but it was more meant to be
used as a building block in other cryptographic protocols.  But most of the
investment around it now is mainly on its use as a social media protocol, since
that's what sells to VCs.  I think it *could* be useful for some future thing
that doesn't exist yet, but I don't think it'll be social media.  Nostr
developers and users tend to [*hate*](https://nostr.com/comparisons/mastodon)
Fedi, with some valid critisisms, but these criticisms are not existential to
the *idea* of the Fediverse and mostly I don't think are as severe as presented
there.

Like most people, I don't think Twitter will have a specific death date.  It'll
just be a slow rot as all the people that make it worth being there leave.  Some
people think that we're simply reaching the end of the era of megaplatforms and
that many smaller platforms will flourish.  But for reasons I outlined in the
reddit article (linked at top of page), whatever comes after this has to be
decentralized and not vulnerable to capture otherwise we'll just repeat the
cycle in ~15 years.  Protocols over platforms.

<!-- tz-insert-footer -->

