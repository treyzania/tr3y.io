# Git gud projects

Here's a list of projects I formerly had on
[the git gud guide](/articles/tech/gitgud.html).  I moved them here because this
list was too long for that page and it distracted from the more general points I
wanted to share.

## Building your castle

So a common process that I instruct people to go through to stretch their legs
with a non-programming hands-on computer task is to deploy a basic website on
VPS they rent, with proper certificates.  It helps to have something like this
as a basic branching off point that lightly touches on several different
practical topics.  It also establishes yourself with a presence on the internet
that you can point to for anyone and publish things on.

* Buy a domain from a registrar like Namecheap or Njalla.  I suggest against .com domains for personal sites, but you can also try something funny with a gTLD.  I also own [foobar.style](https://foobar.style), for example.
  * If you ask *really nicely* I might give you a subdomain on one of my domains so you don't have to buy your own domain, but you'll be dependent on me to update the DNS records and if you're an ass I'll just revoke it.
* Set up an SSH private key (which is very easy on Linux).
* Rent a cheap VPS (~$5/mo) from a provider like DigitalOcean, OVH, Rackspace, Vultr, or maybe Linode.  I **suggest against** using AWS, GCP, Azure, or Oracle Cloud.  I am aware they have good offers, especially for students, but see the Philosophy section below.
  * Instead of renting a VPS, you could do this on your home internet, but there's some caveats. (see last paragraph)
* Create DNS records in the registrar to point your domain to the VPS.
* Connect to the VPS over SSH and install Nginx, copy up a basic web page using SCP.
* Register a certificate for your domain from Let's Encrypt.
* Install and enable fail2ban and ufw to protect your ports.

DigitalOcean has some guides that may help with some of these steps.  There's
also [this website](https://linuxjourney.com/) that provides a broader
collection of resources on specific Linux topics that you could also consider.

You might want to learn some basic HTML to make something that looks nice, but
only if you want to splurge.  Doing these steps tries to gives you a good
foundation for other projects later where you'd benefit from having a server
in the sky to host something for you.

All of these steps can be done more easily and in less time and potentially
for cheaper, if you just use some Amazon/Google/Wix/Squarespace/etc tool.  But
you won't learn nearly as much that way and you'll be locked into some special
corporate offering that will limit what you're able to do and make it harder to
migrate if you decide to leave later.  Doing it quick/cheap here isn't the goal
here, the goal is enrichment.

**Home hosting caveat:** A variation of this section is to do similar steps with
the home server you set up under the "messing with home servers" section, but
many ISPs have restrictions on incoming connections that get in the way of plain
HTTP traffic, so it may be a bit flaky.  I know that RCN/Astound and Cox block
ports 80 and 443, which means HTTPS works but not HTTP.  This is actually fine
for a lot of purposes, but without the automatic redirect to HTTPS people might
think your web server is down so I don't host public-facing services at home
without a proxy tunnel in front of them.

## Messing with your computer

Some of these require that you've made the switch to to Linux.

* set up your `/home` directory onto a seprate partition from your root volume so you can reinstall your OS without losing your personal data

* change your desktop environment theme to suit your fancy, see [/r/unixporn](https://old.reddit.com/r/unixporn)

* change your GRUB color scheme and/or background (see `grub-customizer`)

* play a Windows-only game in WINE or Steam Proton, lol

* set up a `--bare` git repository on your VPS with hooks so you can `git push prod` and have it automatically update the site [like me](https://gitlab.com/treyzania/tr3y.io)

* set up a Conky or Polybar display

## Messing with the internet

The focus of these projects are to work with the internet and *really get* how
computers actually talk to each other.  If you focus too strongly on the web
you become trapped into the client/server model of HTTP, and will think about
everything in those terms.  HTTP is a tool, it can be misused.

* build a chat client and server using raw TCP sockets

  * maybe start in Python, use threads, then later switch to async/await

  * write a compatible client in Rust, trying both with threads and later async/await

  * alternatively, read [Beej's Guide to Network Programming](https://beej.us/guide/bgnet/) and do it in C

  * eventually, build a GUI for it in GTK or Qt

* add encryption to your chat protocol

  * you can use SSL/TLS, but maybe you should use NaCl and try to make it end-to-end encrypted

* build a web server from scratch (starting point: run `nc -l -p 8123` and connect to http://localhost:8123 and see what happens)

* build an interesting website with a framework like FastAPI or Rocket

  * deploy it behind an nginx server

  * learn how to deploy it using Docker

  * use a database engine like PostgreSQL if you need to store more data

* set up a ZNC server, connect to it with HexChat/irssi, and make friends on IRC

// TODO restructure this section, maybe split out 

## Messing with systems

* build a shell, with support for pipes and IO redirection

* build a garbage collector (probably in C) (hard!)

* use an assembly language (do it in conjunction with C) (harder!)

// TODO restructure this section

## Messing with languages and automata

By "languages" here I'm referring to "formal languages", which includes
programming languages but is more broad.  Some of these you might want to
consider focusing on the more functional languages due to their strengths.

* learn [regex](https://regex101.com/) (easy!)

* learn about [different kinds of automata](https://en.wikipedia.org/wiki/Automata_theory), maybe implement an evaluator

* learn how parsers work

  * write a recursive descent parser for a simple syntax like JSON

* learn how interpreters work, see [Crafting Interpreters](https://craftinginterpreters.com/)

  * build a Lisp-like language that can evaluate mathematical expressions

  * then, add support for variable binding to it, then add lexical scoping that properly handles shadowing

* learn how compilers work, see [the dragon book](https://en.wikipedia.org/wiki/Compilers:_Principles,_Techniques,_and_Tools) or [the tiger book](https://www.cs.princeton.edu/~appel/modern/ml/)

  * (see previous subbullets)

* build a stack machine interpreter

  * then, write some programs in it

  * then, write a compiler from something like SSA to it

  * then, extend it to (efficiently!) support nondetermistic execution, and write a program that takes advantage of it (fun!)

* build a Turing machine interpreter

  * (see previous subbullets)

* implement [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

* implement a [brainfuck](https://en.wikipedia.org/wiki/Brainfuck) interpreter

  * then, write a brainfuck *compiler* (easier if you do it to C and pass it to a C compiler)

  * then, make it and *optimizing* compiler

## Messing with game engines and graphics libraries

* play with [Godot](https://godotengine.org/)

* experiment with procedural generation, it doens't have to be as cool as Minecraft

* make a multiplayer game using its built-in tools

* make a multiplayer game using a game server you built yourself

  * then, use UDP for handling realtime player movement instead of TCP

* learn how to use OpenGL and draw a graphics accelerated triangle

  * then, write a basic model loader and display the Utah teapot and/or Suzanne

## Messing with home servers

Some of these you could choose to run in Docker.  Docker can make service
management a lot easier, but it comes with its own tradeoffs like making
integration between different services more difficult.  Nginx and Nextcloud are
good examples of services I find a lot easier to *not* run in Docker.  These can
be easily installed through your system package manager.

* build a cheap home server out of an old desktop/laptop (or maybe buy a Librem Mini or some other mini PC)

  * point a subdomain at your home IP address
  
  * re-run some of the same steps from the "castle" section at the top to set up a basic web frontend with Nginx or Apache

  * if your IP address changes often, write a script to update the DNS record in Namecheap

* run a PiHole

* set up a RAID for your `/home` directory and anything else you need

* run a Minecraft server

  * understand port forwarding on your router

  * probably actually want to use PaperSpigot, not the vanilla jar

* install Nextcloud on it (if your ISP allows port 443)

  * make sure you have a certificate!

  * then, set up contact and calendar sync in your desktop environment with it

  * then, set up contact and calendar sync to your phone via CardDAV/CalDAV

* set up a remotely controllable torrent client like Transmission

  * set up a VPN around it with [vpnwrap](https://code.tr3y.io/treyzania/vpnwrap/), use Mullvad or maybe NordVPN or something

  * then, build something to notify you on your phone when it's finished downloading a torrent

* build a personal VPN with WireGuard to securely connect your home server and your cloud server

  * if your home network is behind a cgNAT, this can help with dealing with that by providing you another way into your home network
  
* run a Bitcoin/Ethereum/etc full node, be mindful of storage usage and make sure to prune if/when needed

* run an Yggdrasil or a cjdns node

* redo everything you've done on your server with Ansible (and test it) so that you can redeploy everythng on new hardware with a new OS installation

